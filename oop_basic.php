<?php
class StudentInfo{
    public $std_id;//property of class
    public $std_name;//property of class
    public $std_cgpa;//property of class
    public function set_std_id($std_id){//method of class
      $this->std_id=$std_id;//$this is a special type of variable which is used to assign value of local variable to global variable
                     }
    public function set_std_name($std_name){//method of class
        $this->std_name=$std_name;
    }
    public function set_std_cgpa($std_cgpa){//method of class
        $this->std_cgpa=$std_cgpa;
    }
    public function get_std_id(){
        return $this->std_id;
        }
    public function get_std_name(){
        return $this->std_name;
    }
    public function get_std_cgpa(){
        return $this->std_cgpa;
    }
}
$obj=new StudentInfo;
$obj->set_std_id("1001");
$result= $obj->get_std_id();
$obj->set_std_name("Farhana");
$name=$obj->get_std_name();
$obj->set_std_cgpa("4.00");
$cgpa= $obj->get_std_cgpa();
echo $result. $name.$cgpa;
?>

